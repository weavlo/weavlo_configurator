# weavlo_configurator

A tool for managing ERPNext configuration data.

## Background

ERPNext has a *lot* of configuration data, stored in many places.

* Paths to repositories (Frappe, ERPNext, Frappe Charts, Frappe PDFKit, etc.)
* Operating system path to Bench, Frappe app, ERNext app, public assets (CSS,JS), etc.
* Global siteconfig.json
* Nginx.conf
* Supervisor.conf
* common_site_config.json
* Site-specific  site_config.json
* apps.txt (list of apps installed)
* currentsite.txt
* Procfile
* Mode (Production, or not)
* Redis server IP and Port numbers
* MySQL server IP and Port numbers.
* User Account that web service is running under

Much more, which need to be identified and documented.

## Purpose
Can we build a program that:
1. Allows us to **view** all this configuration information, from a single place (web page)
2. Allows us to **manage** this configuration information, from a single place (CLI, web page)

Bench does *some* of the 2nd bullet point.  But it's poorly documented, and not well-understood.

## Challenges
We can break some Bench code.  But we cannot modify/replace anything that might break Frappe and ERPNext code.

So, assume we build our own configuration repository or rules.  If we do this, we also must do 1 of 2 things:
* Make sure our configuration changes are **synchronized** to ERPNext file structure.
* Submit PRs to Frappe/ERPNext/Bench, so that they share our configurations.

This could get tricky.  But it's worth the effort, to have a nice Configuration Tool.

## Related Thoughts
Brian Pond's fork of Bench allowed you to change GIt paths, in case you want to run ERPNext from your own fork.  Let's take that Bench code, and refactor it.

Think about Firefox, and its *about:config*.  Simple, but effective.  A web page with Key-Values where you can edit configurations.

We already have [Redis](https://redis.io/).  Could we build on that, and use Redis for storing more configuration data?